var express = require('express');
var app = express();
var bodyParser = require('body-parser'); //Se utiliza para parsear el json
app.use(bodyParser.json());
var port = process.env.PORT || 3000;

var expirasesion = 1800000; // El tiempo de expiración de la sesión es de 30 minutos, 1800000 milisegundos
var expiracodigo = 300000;  // El tiempo de expiración del código de reactivación es de 5 minutos, 300000 milisegundos

var jwt = require('jwt-simple');
var clavejwt = "jwtproyectotechupsjg"

//Variables para gestionar el acceso a MongoDB con mLab
var proyMlabURL = "https://api.mlab.com/api/1/databases/proyectopsj/collections/";
var mLabAPIKey = "apiKey=RIRPGgUWpSCGy3HguQXwGddMOliniFZr";
var requestJson = require('request-json');

app.listen(port);
console.log("API escuchando en el puerto " + port);

app.get('/apitechu/v1',
  function(req, res) {
    console.log("GET /apitechu/v1");
    res.send(
      {
        "msg" : "Bienvenido a la API de TechU"
      }
    );
  }
);

// Devuelve las cuentas de un cliente
app.get('/apitechu/v1/cliente/:nif/cuenta',
  function(req, res) {
    console.log("GET /apitechu/v1/cliente/:nif/cuenta");

    var token = req.headers.token;
    var payload = validarToken(token);

    if (payload.status == "401") {
      res.status(401);
      response = {
        "msg" : "Usuario no autenticado"
      };
      res.send(response)
    }
    else {
        var nif = req.params.nif;
        var query = 'q={"nif" : "' + nif + '"}';

        httpClient = requestJson.createClient(proyMlabURL);

        httpClient.get("cuenta?" + query + "&" + mLabAPIKey,
        function(err, resMLab, body) {
             if (err) {
               response = {
                 "msg" : "Error obteniendo cuenta."
               }
               res.status(500);
             } else {
               if (body.length > 0) {
                 response = body;
               } else {
                 response = {
                   "msg" : "Cuenta no encontrada."
                 };
                 res.status(404);
               }
             }
             res.send(response);
           }
        )
      }
  }
);

// Crea una nueva cuenta de un cliente
app.post('/apitechu/v1/cliente/:nif/cuenta',
  function(req, res) {
    console.log("POST /apitechu/v1/cliente/:nif/cuenta");

    var token = req.headers.token;
    var payload = validarToken(token);

    if (payload.status == "401") {
      res.status(401);
      response = {
        "msg" : "Usuario no autenticado"
      };
      res.send(response)
    }
    else {
          // En primer lugar damos de alta la cuenta
          var iban = generarIban();
          var putBody ={
            "iban" : iban,
            "nif" : req.params.nif,
            "saldo" : 0
          };

          httpClient = requestJson.createClient(proyMlabURL);

          httpClient.post("cuenta?" + mLabAPIKey, putBody,
          function(errPUT, resMLabPUT, bodyPUT){
               console.log("Cuenta creada");
             }
          )
          // Después damos de alta la cuenta en la colección de movimientos
          var putBody ={
            "iban" : iban
          };

          httpClient.post("movimiento?" + mLabAPIKey, putBody,
          function(errPUT, resMLabPUT, bodyPUT){
             }
          )
          response = {
            "resultado" : "OK",
            "iban" : iban
          };
          res.send(response);
      }
  }
);

// Devuelve los movimientos de una cuenta
app.get('/apitechu/v1/cuenta/:iban/movimiento',
  function(req, res) {
    console.log("GET /apitechu/v1/cuenta/:iban/movimiento");
    var token = req.headers.token;
    var payload = validarToken(token);
    if (payload.status == "401") {
      res.status(401);
      response = {
        "msg" : "Usuario no autenticado"
      };
      res.send(response)
    } else {

        var iban = req.params.iban;
        var query = 'q={"iban" : "' + iban + '"}';

        httpClient = requestJson.createClient(proyMlabURL);

        httpClient.get("movimiento?" + query + "&" + mLabAPIKey,
        function(err, resMLab, body) {
             if (err) {
               response = {
                 "msg" : "Error obteniendo cuenta."
               }
               res.status(500);
             } else {
               if (body.length > 0) {
                 var importedesde = req.headers.importedesde || 0;
                 var importehasta = req.headers.importehasta || 9999999;
                 var hoy = Date.parse(new Date());
                 var fechadesde = Date.parse(req.headers.fechadesde) || 0;
                 var fechahasta = Date.parse(req.headers.fechahasta) || hoy;
                 fechahasta = fechahasta + 86400000;
                 if (body[0].movimiento == undefined) {
                   movimientos = []
                 } else {
                   movimientos = filtrarMovimientos(body[0].movimiento, importedesde, importehasta, fechadesde, fechahasta);
                 }
                 response = {
                   "iban" : body[0].iban,
                   "importedesde" : importedesde,
                   "importehasta" : importehasta,
                   "fechadesde" : req.headers.fechadesde,
                   "fechahasta" : req.headers.fechahasta,
                   "movimiento" : movimientos
                 };
               } else {
                 response = {
                   "msg" : "Cuenta no encontrada."
                 };
                 res.status(404);
               }
             }
             res.send(response);
           }
        )
      }
  }
);

// Crea un nuevo movimiento en una cuenta
app.post('/apitechu/v1/cuenta/:iban/movimiento',
  function(req, res) {
    console.log("POST /apitechu/v1/cuenta/:iban/movimiento");

    var token = req.headers.token;
    var payload = validarToken(token);

    if (payload.status == "401") {
      res.status(401);
      response = {
        "msg" : "Usuario no autenticado"
      };
      res.send(response)
    } else {

        var iban = req.params.iban;
        var tipo = req.body.tipo;
        var importe = parseFloat(req.body.importe) || 0;
        var detalle = req.body.detalle;
        var fecha = new Date();
        var query = 'q={"iban" : "' + iban + '"}';

        httpClient = requestJson.createClient(proyMlabURL);
        console.log("Consulta cliente");

        httpClient.get("cuenta?" + query + "&" + mLabAPIKey,
        function(err, resMLab, body) {
             if (err) {
               response = {
                 "msg" : "Error obteniendo cuenta."
               }
               res.status(500);
             } else {
               if (body.length > 0) {
                 var saldo = body[0].saldo;
                 var operacionok = true;
                 switch (tipo){
                    case "A":
                      nuevosaldo = saldo + importe;
                      break;
                    case "C":
                      if (importe > saldo){
                        operacionok = false;
                        response = {
                          "resultado" : "KO"
                        }
                      }  else {
                          nuevosaldo = saldo - importe;
                          importe = 0 - importe;
                        }
                      break;
                    default:
                      operacionok = false;
                      response = {
                        "msg" : "Tipo de operación incorrecto."
                      }
                 }
                 if (operacionok) {
                   query = 'q={"iban" : "' + body[0].iban +'"}';
                   var putBody = '{"$set": {"saldo" : ' + nuevosaldo + '}}';

                   httpClient.put("cuenta?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
                   function(errPUT, resMLabPUT, bodyPUT){
                        console.log("El saldo se ha actualizado");
                      }
                   )

                   var putBody = '{"$push": {"movimiento": { "importe" : ' + importe + ', "detalle" : "' + detalle + '", "fecha" : "' + fecha + '"}}}';

                   httpClient.put("movimiento?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
                   function(errPUT, resMLabPUT, bodyPUT){
                        console.log("El movimiento se ha realizado");
                      }
                   )


                   response = {
                     "resultado" : "OK",
                     "iban" : body[0].iban,
                     "saldo" : nuevosaldo
                   };
                 }
               } else {
                 response = {
                   "msg" : "Cuenta no encontrada."
                 };
                 res.status(404);
               }
             }
             res.send(response);
           }
        )
      }
  }
);

// Devuelve la información de un cliente
app.get('/apitechu/v1/cliente/:nif',
  function(req, res) {
    console.log("GET /apitechu/v1/cliente/:nif");

    var token = req.headers.token;
    var payload = validarToken(token);

    if (payload.status == "401") {
      res.status(401);
      response = {
        "msg" : "Usuario no autenticado"
      };
      res.send(response)
    } else {

        var nif = req.params.nif;
        var query = 'q={"nif" : "' + nif + '"}';

        httpClient = requestJson.createClient(proyMlabURL);

        httpClient.get("cliente?" + query + "&" + mLabAPIKey,
        function(err, resMLab, body) {
             if (err) {
               response = {
                 "msg" : "Error obteniendo cliente."
               }
               res.status(500);
             } else {
               if (body.length > 0) {
                 response = body[0];
               } else {
                 response = {
                   "msg" : "Cliente no encontrado."
                 };
                 res.status(404);
               }
             }
             res.send(response);
           }
        )
      }
  }
);


// Crea un nuevo cliente
app.post('/apitechu/v1/cliente',
  function(req, res) {
    console.log("POST /apitechu/v1/cliente");

    // Primero consultamos si ya existe su NIF
    var nif = req.body.nif;
    var query = 'q={"nif" : "' + nif + '"}';

    httpClient = requestJson.createClient(proyMlabURL);

    httpClient.get("cliente?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body) {
         if (body.length > 0) {
           response = {
             "msg" : "KO"
           };
         } else {
                   // Como el cliente no existe, lo creamos
                   var iban = generarIban();
                   var putBody = {
                     "nif" : req.body.nif,
                     "nombre" : req.body.nombre,
                     "apellido1" : req.body.apellido1,
                     "apellido2" : req.body.apellido2,
                     "email" : req.body.email,
                     "password" : req.body.password,
                     "cuenta" : [iban]
                   };

                   httpClient = requestJson.createClient(proyMlabURL);

                   httpClient.post("cliente?" + mLabAPIKey, putBody,
                   function(errPUT, resMLabPUT, bodyPUT){
                        console.log("Cliente creado");
                      }
                   )
                   // Después de dar de alta el cliente damos de alta la cuenta
                   var putBody ={
                     "iban" : iban,
                     "nif" : req.body.nif,
                     "saldo" : 0
                   };

                   httpClient.post("cuenta?" + mLabAPIKey, putBody,
                   function(errPUT, resMLabPUT, bodyPUT){
                        console.log("Cuenta creada");
                      }
                   )

                   // Finalmente damos de alta la cuenta en la colección de movimientos
                   var putBody ={
                     "iban" : iban
                   };

                   httpClient.post("movimiento?" + mLabAPIKey, putBody,
                   function(errPUT, resMLabPUT, bodyPUT){
                      }
                   )

                   response = {
                     "msg" : "OK",
                     "cliente" : {
                       "nif" : req.body.nif,
                       "nombre" : req.body.nombre,
                       "apellido1" : req.body.apellido1,
                       "apellido2" : req.body.apellido2,
                       "cuenta" : iban
                     }
                   };
                 }
         res.send(response);
       }
    )
  }
);


// Login de un cliente
app.post('/apitechu/v1/login',
  function(req, res) {
    console.log("POST /apitechu/v1/login");

    var nif = req.body.nif;
    var password = req.body.password;
    var query = 'q={"nif" : "' + nif + '", "password" : "' + password + '"}';

    httpClient = requestJson.createClient(proyMlabURL);

    httpClient.get("cliente?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body) {
         if (err) {
           response = {
             "msg" : "Error obteniendo usuario."
           }
           res.status(500);
         } else {
           if (body.length > 0) {
             var fecha = new Date();
             var payload = {
               "nif" : nif,
               "fecha" : fecha
             };
             response = {
               "token": generarToken(payload),
               "nif": body[0].nif,
               "nombre": body[0].nombre,
               "apellido1": body[0].apellido1,
               "apellido2": body[0].apellido2
             };
           } else {
             response = {
               "msg" : "Usuario o clave incorrecta."
             };
             res.status(404);
           }
         }
         res.send(response);
       }
    )

  }
);

// Olvidó su clave. Envío de código de reactivación
app.put('/apitechu/v1/cliente/:nif/clave',
  function(req, res) {
    console.log("PUT /apitechu/v1/cliente/:nif/clave");

    var nif = req.params.nif;
    var query = 'q={"nif" : "' + nif + '"}';

    httpClient = requestJson.createClient(proyMlabURL);

    httpClient.get("cliente?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body) {
       if (body.length > 0) {
         var email = body[0].email;
         var nodemailer = require('nodemailer');

         var codigo = Math.round(Math.random() * 900000 + 100000);
         var feccodigo = new Date();

         query = 'q={"_id" : ' + JSON.stringify(body[0]._id) +'}';
         var putBody = '{"$set":{"codigoreset":' + codigo + ', "expirareset":"' + feccodigo + '"}}';

         httpClient.put("cliente?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
         function(errPUT, resMLabPUT, bodyPUT){
              console.log("Código creado");
            }
         )

         var transporter = nodemailer.createTransport({
           service: 'gmail',
           auth: {
             user: 'proyectotechu@gmail.com',
             pass: 'dcqaymlmP0'
           }
         });

         var mailOptions = {
           from: 'proyectotechu@gmail.com',
           to: email,
           subject: 'Reseteo de password',
           text: 'El código para resetear su password es el siguiente: ' + codigo + '. Tiene validez durante 5 minutos.'
         };

         transporter.sendMail(mailOptions, function(error, info){
           if (error) {
             console.log(error);
           } else {
            //  console.log('Email sent: ' + info.response);
           }
         });
        }
         res.send(
            {
               "msg" : "Se enviará un correo a su dirección email."
            }
          );
       }
    )

  }
);

// Reactivación clave
app.put('/apitechu/v1/cliente/:nif/reactivacion',
  function(req, res) {
    console.log("PUT /apitechu/v1/cliente/:nif/reactivacion");

    var nif = req.params.nif;
    var codigointro = req.body.codigo;
    var fecintro = new Date();
    var query = 'q={"nif" : "' + nif + '"}';

    httpClient = requestJson.createClient(proyMlabURL);

    //En primer lugar recupera la información del cliente
    httpClient.get("cliente?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body) {
         if (err) {
           response = {
             "msg" : "Error obteniendo usuario."
           }
           res.status(500);
         } else {
           if (body.length > 0) {
             var codigoreset = body[0].codigoreset;
             var expirareset = body[0].expirareset;
             if (codigointro != codigoreset) {
               response = {
                 "msg" : "CI"
               };
             } else {
               if ((Date.parse(fecintro) - Date.parse(expirareset)) > expiracodigo) {
                 response = {
                   "msg" : "CE"
                 };
               } else {
                 // Código de reactivación correcto y válido
                 var password =  req.body.password;
                 var putBody = '{"$set":{"password": "' + password + '"}, "$unset":{"codigoreset":"", "expirareset":""}}';

                 httpClient.put("cliente?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
                 function(errPUT, resMLabPUT, bodyPUT){
                      console.log("Password reactivada");
                    }
                 )
                response = {
                  "msg" : "Password reactivada.",
                  "cliente" : {
                    "nif" : body[0].nif,
                    "nombre" : body[0].nombre,
                    "apellido1" : body[0].apellido1,
                    "apellido2" : body[0].apellido2,
                    "cuenta" : body[0].cuenta
                  }
                };
               }
             }
           } else {
             response = {
               "msg" : "Usuario no encontrado."
             };
             res.status(404);
           }
         }
         res.send(response);
       }
    )
  }
);

// Modificación de un cliente
app.put('/apitechu/v1/cliente/:nif',
  function(req, res) {
    console.log("PUT /apitechu/v1/cliente/:nif");

    var token = req.headers.token;
    var payload = validarToken(token);

    if (payload.status == "401") {
      res.status(401);
      response = {
        "msg" : "Usuario no autenticado"
      };
      res.send(response)
    } else {

        var nif = req.params.nif;
        var query = 'q={"nif" : "' + nif + '"}';

        httpClient = requestJson.createClient(proyMlabURL);

        //En primer comprueba que el cliente existe
        httpClient.get("cliente?" + query + "&" + mLabAPIKey,
        function(err, resMLab, body) {
             if (err) {
               response = {
                 "resultado" : "KO"
               }
               res.status(500);
             } else {
               if (body.length > 0) {
                 var nombre = req.body.nombre;
                 var apellido1 = req.body.apellido1;
                 var apellido2 = req.body.apellido2;
                 var email = req.body.email;
                 var password = req.body.password;
                 var putBody = '{"$set":{"nombre": "' + nombre + '", "apellido1": "' + apellido1 + '", "apellido2": "' + apellido2 + '", "email": "' + email + '", "password": "' + password + '"}}';

                 httpClient.put("cliente?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
                 function(errPUT, resMLabPUT, bodyPUT){
                      console.log("Usuario modificado");
                    }
                 )
                response = {
                  "resultado" : "OK"
                  }
               } else {
                 response = {
                   "resultado" : "KO"
                 };
                 res.status(404);
               }
             }
             res.send(response);
           }
        )
      }
  }
);

// Devuelve la información de las oficinas
app.get('/apitechu/v1/oficina',
  function(req, res) {
    console.log("GET /apitechu/v1/oficina");

    var token = req.headers.token;
    var payload = validarToken(token);

    if (payload.status == "401") {
      res.status(401);
      response = {
        "msg" : "Usuario no autenticado"
      };
      res.send(response)
    } else {

        httpClient = requestJson.createClient(proyMlabURL);

        httpClient.get("oficina?&" + mLabAPIKey,
        function(err, resMLab, body) {
             if (err) {
               response = {
                 "msg" : "Error obteniendo oficinas."
               }
               res.status(500);
             } else {
               if (body.length > 0) {
                 response = body;
               } else {
                 response = {
                   "msg" : "No existen oficinas."
                 };
                 res.status(404);
               }
             }
             res.send(response);
           }
        )
      }
  }
);

// Devuelve la lista de divisas
app.get('/apitechu/v1/divisa',
  function(req, res) {
    console.log("GET /apitechu/v1/divisa");

    var token = req.headers.token;
    var payload = validarToken(token);

    if (payload.status == "401") {
      res.status(401);
      response = {
        "msg" : "Usuario no autenticado"
      };
      res.send(response)
    } else {

        httpClient = requestJson.createClient(proyMlabURL);

        httpClient.get("divisa?&" + mLabAPIKey,
        function(err, resMLab, body) {
             if (err) {
               response = {
                 "msg" : "Error obteniendo divisas."
               }
               res.status(500);
             } else {
               if (body.length > 0) {
                 response = body;
               } else {
                 response = {
                   "msg" : "No existen divisas."
                 };
                 res.status(404);
               }
             }
             res.send(response);
           }
        )
      }
  }
);

// Genera el Token de sesión
function generarToken(payload) {
  var token = jwt.encode(payload, clavejwt);
  return token;
}

// Valida el Token de sesión
function validarToken(token) {
  if (token == "") {
    // El token llega vacío
    console.log("Token vacío")
    return ({
      "status": "401"
    });
  } else {
    try {
      var payload = jwt.decode(token, clavejwt);
    } catch(e) {
      // El token es erróneo
      console.log("Token erróneo")
      return ({
        "status": "401"
      });
    }

    var fechalogin = Date.parse(payload.fecha);
    var fechaactual = Date.parse(new Date());
    if (fechaactual - fechalogin > expirasesion) {
      console.log("La sesión ha expirado")
      return ({
        "status": "401"
      });
    }
    return({
      "status": "200",
      "payload": payload
    });
  }
}

function generarIban() {
  var codES = Math.round(Math.random() * 90 + 10);
  var codOficina = Math.round(Math.random() * 9000 + 1000);
  var digitoControl = Math.round(Math.random() * 90 + 10);
  var folio = Math.round(Math.random() * 9000000 + 1000000);
  var iban = "ES" + codES + "0182" + codOficina + digitoControl + "020" + folio;
  return iban;
}

function filtrarMovimientos(movimientos, importedesde, importehasta, fechadesde, fechahasta) {
  var nuevolistado = movimientos.filter(filtro => {
    if (Math.abs(filtro.importe) >= importedesde && Math.abs(filtro.importe) <= importehasta && Date.parse(filtro.fecha) >= fechadesde && Date.parse(filtro.fecha) <=fechahasta) {
      return true
    } else {
      return false
    }
  });
  return nuevolistado
}
