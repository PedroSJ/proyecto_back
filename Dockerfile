# Imagen raíz
FROM node

# Carpeta raíz
WORKDIR /apitechu

# Copia de archivos
ADD . /apitechu

# Exponer puerto
EXPOSE 3000

# Instalar dependencias. Poner si no existe .dockerignore
RUN npm install

#Comando de inicialización
CMD ["npm", "start"]
