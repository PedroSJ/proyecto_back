var mocha = require('mocha');
var chai = require('chai');
var chaihttp = require('chai-http');

var server = require('../server.js');

chai.use(chaihttp);

var should = chai.should();
var token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJuaWYiOiIyM1QiLCJmZWNoYSI6IjIwMTgtMDUtMThUMTY6MTg6NDcuODEwWiJ9.FOZeMtuyWVYN2sZbdgfeQm_4u7XNtP5AuO7WrlVJzZ8';
var nif = "23T";
var iban = "ES5501821000010200112233";

describe('Test general de API',
 function() {
   it('Prueba que el servidor responde.',
     function(done) {
       chai.request('http://localhost:3000')
         .get('/apitechu/v1')
         .end(
           function(err, res) {
             res.should.have.status(200);
             res.body.msg.should.be.eql("Bienvenido a la API de TechU")
             done();
           }
         )
     }
   )
 }
);

describe('Test API /apitechu/v1/divisa',
 function() {
   it('Prueba que la API devuelve una lista de divisas.',
     function(done) {
       chai.request('http://localhost:3000')
       .get('/apitechu/v1/divisa')
       .set({'token' : token})
       .end(
         function(err, res) {
           res.should.have.status(200);
           res.body.should.be.a("array");
           for (divisa of res.body) {
             divisa.should.have.property('id');
             divisa.should.have.property('nombre');
           }
           done();
         }
       )
     }
   )
   it('Prueba que la API un error 401 si el token no es válido.',
     function(done) {
       chai.request('http://localhost:3000')
       .get('/apitechu/v1/divisa')
       .set({'token' : 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJuaWYiOiIyM1QiLCJmZWNoYSI6IjIwMTgtMDUtMDNUMTI6NTI6MDIuMzE2WiJ9.HYfXsDqJr8eRnf1OmSpjFSMM9L7DxL0IIwVusmrNZTa'})
       .end(
         function(err, res) {
           res.should.have.status(401);
           done();
         }
       )
     }
   )
   it('Prueba que la API un error 401 si no se envía token.',
     function(done) {
       chai.request('http://localhost:3000')
       .get('/apitechu/v1/divisa')
       .end(
         function(err, res) {
           res.should.have.status(401);
           done();
         }
       )
     }
   )
 }
);

describe('Test API /apitechu/v1/cliente/:nif',
 function() {
   it('Prueba que la API devuelve la información del cliente.',
     function(done) {
       chai.request('http://localhost:3000')
       .get('/apitechu/v1/cliente/' + nif)
       .set({'token' : token})
       .end(
         function(err, res) {
           res.should.have.status(200);
           res.body.should.have.property('nif');
           res.body.should.have.property('nombre');
           res.body.should.have.property('apellido1');
           res.body.should.have.property('apellido2');
           res.body.should.have.property('email');
           res.body.should.have.property('password');
           done();
         }
       )
     }
   )
   it('Prueba que la API un error 401 si el token no es válido.',
     function(done) {
       chai.request('http://localhost:3000')
       .get('/apitechu/v1/cliente/' + nif)
       .set({'token' : 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJuaWYiOiIyM1QiLCJmZWNoYSI6IjIwMTgtMDUtMDNUMTI6NTI6MDIuMzE2WiJ9.HYfXsDqJr8eRnf1OmSpjFSMM9L7DxL0IIwVusmrNZTa'})
       .end(
         function(err, res) {
           res.should.have.status(401);
           done();
         }
       )
     }
   )
   it('Prueba que la API un error 401 si no se envía token.',
     function(done) {
       chai.request('http://localhost:3000')
       .get('/apitechu/v1/cliente/' + nif)
       .end(
         function(err, res) {
           res.should.have.status(401);
           done();
         }
       )
     }
   )
 }
);

describe('Test API /apitechu/v1/cuenta/:iban/movimiento',
 function() {
   it('Prueba que la API devuelve los movimientos de la cuenta.',
     function(done) {
       chai.request('http://localhost:3000')
       .get('/apitechu/v1/cuenta/' + iban + '/movimiento')
       .set({'token' : token})
       .end(
         function(err, res) {
           res.should.have.status(200);
           res.body.should.have.property('iban');
           res.body.should.have.property('importedesde');
           res.body.should.have.property('importehasta');
           res.body.should.have.property('movimiento');
           done();
         }
       )
     }
   )
   it('Prueba que la API un error 401 si el token no es válido.',
     function(done) {
       chai.request('http://localhost:3000')
       .get('/apitechu/v1/cuenta/' + iban + '/movimiento')
       .set({'token' : 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJuaWYiOiIyM1QiLCJmZWNoYSI6IjIwMTgtMDUtMDNUMTI6NTI6MDIuMzE2WiJ9.HYfXsDqJr8eRnf1OmSpjFSMM9L7DxL0IIwVusmrNZTa'})
       .end(
         function(err, res) {
           res.should.have.status(401);
           done();
         }
       )
     }
   )
   it('Prueba que la API un error 401 si no se envía token.',
     function(done) {
       chai.request('http://localhost:3000')
       .get('/apitechu/v1/cuenta/' + iban + '/movimiento')
       .end(
         function(err, res) {
           res.should.have.status(401);
           done();
         }
       )
     }
   )
 }
);

describe('Test API /apitechu/v1/oficina',
 function() {
   it('Prueba que la API devuelve una lista de oficinas.',
     function(done) {
       chai.request('http://localhost:3000')
       .get('/apitechu/v1/oficina')
       .set({'token' : token})
       .end(
         function(err, res) {
           res.should.have.status(200);
           res.body.should.be.a("array");
           for (oficina of res.body) {
             oficina.should.have.property('codigo');
             oficina.should.have.property('direccion');
             oficina.should.have.property('provincia');
             oficina.should.have.property('cp');
             oficina.should.have.property('localidad');
             oficina.should.have.property('nombre');
           }
           done();
         }
       )
     }
   )
   it('Prueba que la API un error 401 si el token no es válido.',
     function(done) {
       chai.request('http://localhost:3000')
       .get('/apitechu/v1/oficina')
       .set({'token' : 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJuaWYiOiIyM1QiLCJmZWNoYSI6IjIwMTgtMDUtMDNUMTI6NTI6MDIuMzE2WiJ9.HYfXsDqJr8eRnf1OmSpjFSMM9L7DxL0IIwVusmrNZTa'})
       .end(
         function(err, res) {
           res.should.have.status(401);
           done();
         }
       )
     }
   )
   it('Prueba que la API un error 401 si no se envía token.',
     function(done) {
       chai.request('http://localhost:3000')
       .get('/apitechu/v1/oficina')
       .end(
         function(err, res) {
           res.should.have.status(401);
           done();
         }
       )
     }
   )
 }
);

describe('Test API /apitechu/v1/cliente/:nif/cuenta',
 function() {
   it('Prueba que la API devuelve las cuentas de un cliente.',
     function(done) {
       chai.request('http://localhost:3000')
       .get('/apitechu/v1/cliente/' + nif + '/cuenta')
       .set({'token' : token})
       .end(
         function(err, res) {
           res.should.have.status(200);
           res.body.should.be.a("array");
           for (cuenta of res.body) {
             cuenta.should.have.property('iban');
             cuenta.should.have.property('saldo');
             cuenta.should.have.property('nif');
           }
           done();
         }
       )
     }
   )
   it('Prueba que la API un error 401 si el token no es válido.',
     function(done) {
       chai.request('http://localhost:3000')
       .get('/apitechu/v1/cliente/' + nif + '/cuenta')
       .set({'token' : 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJuaWYiOiIyM1QiLCJmZWNoYSI6IjIwMTgtMDUtMDNUMTI6NTI6MDIuMzE2WiJ9.HYfXsDqJr8eRnf1OmSpjFSMM9L7DxL0IIwVusmrNZTa'})
       .end(
         function(err, res) {
           res.should.have.status(401);
           done();
         }
       )
     }
   )
   it('Prueba que la API un error 401 si no se envía token.',
     function(done) {
       chai.request('http://localhost:3000')
       .get('/apitechu/v1/cliente/' + nif + '/cuenta')
       .end(
         function(err, res) {
           res.should.have.status(401);
           done();
         }
       )
     }
   )
 }
);

describe('Test API /apitechu/v1/cliente/:nif/cuenta',
 function() {
   it('Prueba que la API crea una cuenta.',
     function(done) {
       chai.request('http://localhost:3000')
       .post('/apitechu/v1/cliente/' + nif + '/cuenta')
       .set({'token' : token})
       .end(
         function(err, res) {
           res.should.have.status(200);
           res.body.should.have.property('resultado');
           res.body.should.have.property('iban');
           done();
         }
       )
     }
   )
   it('Prueba que la API un error 401 si el token no es válido.',
     function(done) {
       chai.request('http://localhost:3000')
       .post('/apitechu/v1/cliente/' + nif + '/cuenta')
       .set({'token' : 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJuaWYiOiIyM1QiLCJmZWNoYSI6IjIwMTgtMDUtMDNUMTI6NTI6MDIuMzE2WiJ9.HYfXsDqJr8eRnf1OmSpjFSMM9L7DxL0IIwVusmrNZTa'})
       .end(
         function(err, res) {
           res.should.have.status(401);
           done();
         }
       )
     }
   )
   it('Prueba que la API un error 401 si no se envía token.',
     function(done) {
       chai.request('http://localhost:3000')
       .post('/apitechu/v1/cliente/' + nif + '/cuenta')
       .end(
         function(err, res) {
           res.should.have.status(401);
           done();
         }
       )
     }
   )
 }
);

describe('Test API /apitechu/v1/cuenta/:iban/movimiento',
 function() {
   it('Prueba que la API crea un movimiento en la cuenta.',
     function(done) {
       chai.request('http://localhost:3000')
       .post('/apitechu/v1/cuenta/' + iban + '/movimiento')
       .set({'token' : token})
       .send({
              "tipo": "A",
              "importe" : 10.35,
              "detalle" : "Nuevo movimiento"
            })
       .end(
         function(err, res) {
           res.should.have.status(200);
           res.body.should.have.property('resultado');
           res.body.should.have.property('iban');
           res.body.should.have.property('saldo');
           done();
         }
       )
     }
   )
   it('Prueba que la API un error 401 si el token no es válido.',
     function(done) {
       chai.request('http://localhost:3000')
       .post('/apitechu/v1/cuenta/' + iban + '/movimiento')
       .set({'token' : 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJuaWYiOiIyM1QiLCJmZWNoYSI6IjIwMTgtMDUtMDNUMTI6NTI6MDIuMzE2WiJ9.HYfXsDqJr8eRnf1OmSpjFSMM9L7DxL0IIwVusmrNZTa'})
       .end(
         function(err, res) {
           res.should.have.status(401);
           done();
         }
       )
     }
   )
   it('Prueba que la API un error 401 si no se envía token.',
     function(done) {
       chai.request('http://localhost:3000')
       .post('/apitechu/v1/cuenta/' + iban + '/movimiento')
       .end(
         function(err, res) {
           res.should.have.status(401);
           done();
         }
       )
     }
   )
 }
);

describe('Test API /apitechu/v1/cliente',
 function() {
   it('Prueba que la API da de alta un cliente.',
     function(done) {
       chai.request('http://localhost:3000')
       .post('/apitechu/v1/cliente')
       .send({
                "nif": "2300T",
                "nombre": "Pepe",
                "apellido1": "Ruiz",
                "apellido2": "Pérez",
                "email" : "peperuiz@gmail.com",
                "password": "acceso"
            })
       .end(
         function(err, res) {
           res.should.have.status(200);
           res.body.should.have.property('msg');
           res.body.should.have.property('cliente');
           done();
         }
       )
     }
   )
 }
);

describe('Test API /apitechu/v1/cliente/:nif',
 function() {
   it('Prueba que la API modifica los datos del cliente.',
     function(done) {
       chai.request('http://localhost:3000')
       .put('/apitechu/v1/cliente/' + nif)
       .set({'token' : token})
       .send({
                "nombre": "Juan",
                "apellido1": "Gómez",
                "apellido2": "Martín",
                "email": "juan.gomez@gmail.com",
                "password": "c5652d9cc6bcf28e0e20210eae5ef9e54ba35717"
            })
       .end(
         function(err, res) {
           res.should.have.status(200);
           res.body.should.have.property('resultado');
           done();
         }
       )
     }
   )
   it('Prueba que la API un error 401 si el token no es válido.',
     function(done) {
       chai.request('http://localhost:3000')
       .put('/apitechu/v1/cliente/' + nif)
       .set({'token' : 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJuaWYiOiIyM1QiLCJmZWNoYSI6IjIwMTgtMDUtMDNUMTI6NTI6MDIuMzE2WiJ9.HYfXsDqJr8eRnf1OmSpjFSMM9L7DxL0IIwVusmrNZTa'})
       .end(
         function(err, res) {
           res.should.have.status(401);
           done();
         }
       )
     }
   )
   it('Prueba que la API un error 401 si no se envía token.',
     function(done) {
       chai.request('http://localhost:3000')
       .put('/apitechu/v1/cliente/' + nif)
       .end(
         function(err, res) {
           res.should.have.status(401);
           done();
         }
       )
     }
   )
 }
);

describe('Test API /apitechu/v1/login',
 function() {
   it('Prueba que la API realiza el login del cliente.',
     function(done) {
       chai.request('http://localhost:3000')
       .post('/apitechu/v1/login')
       .send({
                "nif": nif,
                "password": "c5652d9cc6bcf28e0e20210eae5ef9e54ba35717"
            })
       .end(
         function(err, res) {
           res.should.have.status(200);
           res.body.should.have.property('token');
           res.body.should.have.property('nif');
           res.body.should.have.property('nombre');
           res.body.should.have.property('apellido1');
           res.body.should.have.property('apellido2');
           done();
         }
       )
     }
   )
 }
);

describe('Test API /apitechu/v1/cliente/:nif/clave',
 function() {
   it('Prueba que la API genera el código y lo envía por correo.',
     function(done) {
       chai.request('http://localhost:3000')
       .put('/apitechu/v1/cliente/' + nif + '/clave')
       .end(
         function(err, res) {
           res.should.have.status(200);
           res.body.should.have.property('msg');
           res.body.msg.should.equal('Se enviará un correo a su dirección email.');
           done();
         }
       )
     }
   )
 }
);

// Este servicio no se puede probar junto con el que genera el código, ya que no se conoce de forma online
describe('Test API /apitechu/v1/cliente/:nif/reactivacion',
 function() {
   it('Prueba que la API da error al enviar un código incorrecto.',
     function(done) {
       chai.request('http://localhost:3000')
       .put('/apitechu/v1/cliente/' + nif + '/reactivacion')
       .send({
            	"nif" : nif,
            	"codigo" : "519215",
            	"password" : "Juan"
            })
       .end(
         function(err, res) {
           res.should.have.status(200);
           res.body.should.have.property('msg');
           res.body.msg.should.equal('CI');
           done();
         }
       )
     }
   )
 }
);
